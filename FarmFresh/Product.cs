using System;

namespace FarmFresh
{
    public class Product
    {
        public string Name { get; set; }

        public string detail { get; set; }

        public string path { get; set; }

        public int Category { get; set; }
    }
}
