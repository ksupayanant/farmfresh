﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FarmFresh.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<ProductController> _logger;

        public ProductController(ILogger<ProductController> logger)
        {
            _logger = logger;
        }



        [HttpGet]
        public List<Product> Get()
        {
            List<Product> productLst = new();
            Product p1 = new();
            p1.Name = "test1";
            p1.detail = "pocket";
            p1.Category = 1;
            p1.path = "assets/screen3/Untitled-1.png";

            Product p2 = new();
            p2.Name = "test2";
            p2.detail = "pocket";
            p2.Category = 2;
            p2.path = "assets/screen3/Untitled-2.png";

            Product p3 = new();
            p3.Name = "test3";
            p3.detail = "pocket";
            p3.Category = 3;
            p3.path = "assets/screen3/Untitled-3.png";

            Product p4 = new();
            p4.Name = "test4";
            p4.detail = "pocket";
            p4.Category = 4;
            p4.path = "assets/screen3/Untitled-4.png";

            Product p5 = new();
            p5.Name = "test5";
            p5.detail = "pocket";
            p5.Category = 5;
            p5.path = "assets/screen3/Untitled-5.png";

            Product p6 = new();
            p6.Name = "test6";
            p6.detail = "pocket";
            p6.Category = 6;
            p6.path = "assets/screen3/Untitled-6.png";

            Product p7 = new();
            p7.Name = "test7";
            p7.detail = "pocket";
            p7.Category = 7;
            p7.path = "assets/screen3/Untitled-7.png";

            Product p8 = new();
            p8.Name = "test8";
            p8.detail = "pocket";
            p8.Category = 8;
            p8.path = "assets/screen3/Untitled-8.png";

            Product p9 = new();
            p9.Name = "test9";
            p9.detail = "pocket";
            p9.Category = 9;
            p9.path = "assets/screen3/Untitled-9.png";

            productLst.Add(p1);
            productLst.Add(p2);
            productLst.Add(p3);
            productLst.Add(p4);
            productLst.Add(p5);
            productLst.Add(p6);
            productLst.Add(p7);
            productLst.Add(p8);
            productLst.Add(p9);

            return productLst;
        }
    }
}
